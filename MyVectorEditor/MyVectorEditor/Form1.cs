﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace MyVectorEditor
{
   
    public partial class Form1 : Form
    {
        int statusButton = 0;
        Bitmap newArea;
        bool click = false;

        public Form1()
        {
            
            InitializeComponent();
            openFileDialog1.Filter = "Изображение (*.png)|*.png|Изображение (*.jpeg)|*.jpeg|All files (*.*)|*.*";
            saveFileDialog1.Filter = "Изображение (*.png)|*.png|Изображение (*.jpeg)|*.jpeg|All files (*.*)|*.*";

            newArea = new Bitmap(newCanvas.Width, newCanvas.Height);
            newCanvas.Image = newArea;


            newCanvas.Width = Screen.PrimaryScreen.WorkingArea.Width;
            newCanvas.Height = Screen.PrimaryScreen.WorkingArea.Height;
        }
        private void ОткрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            if (openFileDialog1.FileName == String.Empty) return;
            try
            {
                newCanvas.Image = Image.FromFile(openFileDialog1.FileName);

            }
            catch (System.IO.FileNotFoundException errorFile)
            {
                MessageBox.Show(errorFile.Message + "\nНет такого файла", "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
            catch (Exception errorFile)
            {
                MessageBox.Show(errorFile.Message, "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            openFileDialog1.ShowDialog();//открытие диалога "открыть файл"
            openFileDialog1.Filter = "Изображение (*.png)|*.png|Изображение (*.jpeg)|*.jpeg|All files (*.*)|*.*";
        }

        private void СоранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            if (openFileDialog1.FileName == String.Empty) return;
            try
            {
                newCanvas.Image = Image.FromFile(openFileDialog1.FileName);

            }
            catch (System.IO.FileNotFoundException errorFile)
            {
                MessageBox.Show(errorFile.Message + "\nНет такого файла", "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
            catch (Exception errorFile)
            {
                MessageBox.Show(errorFile.Message, "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void palette_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();//Создали диалог цвета
            _currentColor.BackColor = colorDialog1.Color;//Закрасили кнопку выбранным цветом
           
        }
        private void СоздатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            colorDialog1.Color = Color.Black;
            fontDialog1.Color = Color.Black;
            _currentColor.BackColor = colorDialog1.Color;
        }

        private void NewCanvas_Click(object sender, EventArgs e)
        {

        }

        private void ВыходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            statusButton = statusButton + 1;
            new DrawPen(newCanvas, colorDialog1);
        }

        private void NewCanvas_MouseMove(object sender, MouseEventArgs e)
        {
           
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ButtonRectangle_Click(object sender, EventArgs e)
        {
            new DrawRectangle(newCanvas, colorDialog1);
        }

        private void ButtonCircle_Click(object sender, EventArgs e)
        {
            new DrawCircle(newCanvas, colorDialog1);
        }
    }
}
