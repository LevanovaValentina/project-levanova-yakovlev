﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyVectorEditor
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        public int _imageX, _imageY;
        public bool statusForm2;
        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
            statusForm2 = true;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            _imageX = Convert.ToInt32(textBox1.Text);
            _imageY = Convert.ToInt32(textBox2.Text);
            this.Close();
            statusForm2 = true;
        }
    }
}
