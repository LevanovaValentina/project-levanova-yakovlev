﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;



namespace MyVectorEditor
{
    class DrawPen:Draw
    {
        private void MouseMove(object sender, MouseEventArgs e)
        {
            if (draw)
            {
                gPen = Graphics.FromImage(newArea);
                gPen.DrawLine(new Pen(colorDialog.Color ,1), x, y, e.X, e.Y);
                x = e.X;
                y = e.Y;
                pictureBox.Image = newArea;
            }

        }
        public DrawPen(PictureBox pictureBox, ColorDialog colorDialog)
        {
            this.pictureBox = pictureBox;
            this.colorDialog = colorDialog;
            pictureBox.MouseDown += MouseDown;
            pictureBox.MouseMove += MouseMove;
            pictureBox.MouseUp += MouseUp;

        }

    }
}
