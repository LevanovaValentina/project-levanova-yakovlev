﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;


namespace MyVectorEditor
{
    class Draw
    {
        public Bitmap newArea;
        public ColorDialog colorDialog;
        public TrackBar trackBar;
        public PictureBox pictureBox;
        public int x, y = 0;
        public bool draw = false;
        public Bitmap newLocalDraw = null;
        public Graphics g = null;
        public Graphics gPen;
        public int mouseX, mouseY = 0;
        public bool IsClicked = false;
        public void MouseDown(object sender, MouseEventArgs e)
        {
            newArea = (Bitmap)pictureBox.Image.Clone();
            draw = true;
            x = e.X;
            y = e.Y;

        }

        public void MouseUp(object sender, MouseEventArgs e)
        {
            draw = false;
        }
    }
}
