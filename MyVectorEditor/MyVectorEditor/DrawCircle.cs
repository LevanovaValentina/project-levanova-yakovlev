﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace MyVectorEditor
{
    class DrawCircle:Draw 
    {
        private void MouseMove(object sender, MouseEventArgs e)
        {
            if (draw)
            {
                int x1 = x < e.X ? x : e.X;
                int x2 = x > e.X ? x : e.X;
                int y1 = x < e.Y ? x : e.Y;
                int y2 = x > e.Y ? x : e.Y;
                Rectangle newRect = new Rectangle(x1, y1, x2 - x1, y2 - y1);
                newLocalDraw = (Bitmap)newArea.Clone();
                g = Graphics.FromImage(newLocalDraw);
                g.DrawEllipse(new Pen(colorDialog.Color, 1), newRect);
                pictureBox.Image = newLocalDraw;
                g.Dispose();
            }


        }
        public DrawCircle(PictureBox pictureBox, ColorDialog colorDialog)
        {
            this.pictureBox = pictureBox;
            this.colorDialog = colorDialog;
            pictureBox.MouseDown += MouseDown;
            pictureBox.MouseMove += MouseMove;
            pictureBox.MouseUp += MouseUp;

        }
    }
}
